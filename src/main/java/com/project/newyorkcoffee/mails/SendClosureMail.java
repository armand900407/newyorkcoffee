package com.project.newyorkcoffee.mails;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import com.project.newyorkcoffee.model.Closure;

public class SendClosureMail {
	    
	public static boolean SendMail(Closure closureData) throws Exception { 
	   
        String bodyClosureMail;
        boolean sentOk=true;
	        bodyClosureMail = MailAuthData.bodyMailClosureBuilder(closureData);
	        
	        Properties props = new Properties();
	        props.put(MailAuthData.SMPT_AUTH, MailAuthData.SMTP_ENABLE);
	        props.put(MailAuthData.SMPT_STARTTLS,  MailAuthData.SMTP_ENABLE);
	        props.put(MailAuthData.SMPT_HOST, MailAuthData.GMAIL_HOST);
	        props.put(MailAuthData.SMTP_PORT, MailAuthData.SMTP_PORT_NUMBER);
	 
	        Session session = Session.getInstance(props,
	                new javax.mail.Authenticator() {
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        try {
								return new PasswordAuthentication(EncryptSecurity.decrypt(MailAuthData.USERNAME), EncryptSecurity.decrypt(MailAuthData.PASSWORD));
							} catch (Exception e) {
								System.out.println(MailAuthData.FAIL_AUTH);
								e.printStackTrace();
							}
							return null;
	                    }
	                });
	 
	        try {
	            
	         // Create the message part
	         BodyPart messageBodyPart = new MimeBodyPart();
	         BodyPart messageBodyPartIncomes= new MimeBodyPart();
	         BodyPart messageBodyPartOutgoings = new MimeBodyPart();
	         BodyPart messageBodyPartClosure = new MimeBodyPart();
	         BodyPart messageBodyPartSales = new MimeBodyPart();
	         
	         // Now set the actual message
	         messageBodyPart.setText(bodyClosureMail);
	         // Create a multipart message
	         Multipart multipart = new MimeMultipart();
	         // Set text message part
	         multipart.addBodyPart(messageBodyPart);

	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(EncryptSecurity.decrypt(MailAuthData.USERNAME)));
	            message.setRecipients(Message.RecipientType.TO,
	                    InternetAddress.parse(EncryptSecurity.decrypt(MailAuthData.DESTINATION)));
	            message.setRecipients(Message.RecipientType.CC,
	                    InternetAddress.parse(MailAuthData.COPY_MAIL_1));
	            message.setSubject(MailAuthData.CLOSURE_NOTIF+closureData.getEmployeeName());
	            message.setText(bodyClosureMail);            
	            // Part two is attachment 
	            //messageBodyPart = new MimeBodyPart();
	            
	            DataSource sourceIncomes = new FileDataSource(MailAuthData.PDF_INCOMES);
	            DataSource sourceOutgoings = new FileDataSource(MailAuthData.PDF_OUTGOINGS);
	            DataSource sourceClosure = new FileDataSource(MailAuthData.PDF_CLOSURE);
	            DataSource sourceSales = new FileDataSource(MailAuthData.PDF_SALES);

	            
	            messageBodyPartIncomes.setDataHandler(new DataHandler(sourceIncomes));
	            messageBodyPartOutgoings.setDataHandler(new DataHandler(sourceOutgoings));
	            messageBodyPartClosure.setDataHandler(new DataHandler(sourceClosure));
	            messageBodyPartSales.setDataHandler(new DataHandler(sourceSales));
	        	
	            //messageBodyPart.setFileName(INCOMES_REPORT);
	            messageBodyPartIncomes.setFileName(MailAuthData.INCOMES_REPORT);
	            messageBodyPartOutgoings.setFileName(MailAuthData.OUTGOINGS_REPORT);
	            messageBodyPartClosure.setFileName(MailAuthData.CLOSURE_REPORT);
	            messageBodyPartSales.setFileName(MailAuthData.SALES_REPORT);
	            
	            //tipart.addBodyPart(messageBodyPart);
	            multipart.addBodyPart(messageBodyPartIncomes);
	            multipart.addBodyPart(messageBodyPartOutgoings);
	            multipart.addBodyPart(messageBodyPartClosure);
	            multipart.addBodyPart(messageBodyPartSales);
	            
	            message.setContent(multipart);
	            Transport.send(message);
	 
	        } catch (MessagingException e) {
	            e.printStackTrace();
	            JOptionPane.showMessageDialog(null,MailAuthData.FAIL_REPORT);
	            sentOk=false;
	            return sentOk;      
	        }
	   return sentOk;
	}
	     
//	     public static void main(String[] args) throws Exception {
//	    	 Closure closure = new Closure();
//	    	 closure.setIdClosure(1);
//	    	 closure.setEmployeeName("LA OING");
//	    	 closure.setInitCash(500.0);
//	    	 closure.setTotalIn(2500.0);
//	    	 closure.setTotalOut(200.0);
//	    	 closure.setTotalCash(2300.0);
//	    	 closure.setTotalCard(150.0);
//	    	 closure.setTotalGenerated(2000.0);
//	    	 closure.setClosureDate("8 Dic 2018");
//	           SendClosureMail.SendMail(closure); 
//	     	}    

}
