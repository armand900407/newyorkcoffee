package com.project.newyorkcoffee.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.project.newyorkcoffee.model.Products;
import com.project.newyorkcoffee.queries.ProductQueries;

@Repository
public class ProductDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	/**
	 * AdministrationRowMapper that works as a rowMaper to get all information related.
	 */
	class ProductRowMapper implements RowMapper<Products> {

		@Override
		public Products mapRow(ResultSet rs, int rowNum) throws SQLException {
			final Products product = new Products();
			product.setIdProduct(rs.getInt(ProductQueries.ID_PRODUCT));
			product.setProductType(rs.getString(ProductQueries.PRODUCT_TYPE));
			product.setProductName(rs.getString(ProductQueries.PRODUCT_NAME));
			product.setSize(rs.getString(ProductQueries.SIZE));
			product.setPrice(rs.getDouble(ProductQueries.PRICE));
			return product;
		}	
	}
	
	/**
	 * findAll finds all the results in the table.
	 */
	public List<Products> findAll() {
		return jdbcTemplate.query(ProductQueries.FIND_ALL, new ProductRowMapper());

	}
	
	/**
	 * findAll finds all the hot drinks results in the table.
	 */
	public List<Products> findHotDrinks() {
		return jdbcTemplate.query(ProductQueries.FIND_HOT_DRINKS, new ProductRowMapper());

	}
	
	/**
	 * findAll finds all the cold drinks results in the table.
	 */
	public List<Products> findColdDrinks() {
		return jdbcTemplate.query(ProductQueries.FIND_COLD_DRINKS, new ProductRowMapper());

	}
	
	/**
	 * findAll finds all the cold drinks results in the table.
	 */
	public List<Products> findCocktailDrinks() {
		return jdbcTemplate.query(ProductQueries.FIND_COCKTAIL_DRINKS, new ProductRowMapper());

	}
	
	/**
	 * findAll finds all the food results in the table.
	 */
	public List<Products> findFood() {
		return jdbcTemplate.query(ProductQueries.FIND_FOOD, new ProductRowMapper());

	}
	
	/**
	 * findAll finds all the food results in the table.
	 */
	public List<Products> findDrinkComplement() {
		return jdbcTemplate.query(ProductQueries.FIND_DRINK_COMPLEMENT, new ProductRowMapper());

	}
	
	/**
	 * findAll finds all the food results in the table.
	 */
	public List<Products> findFoodComplement() {
		return jdbcTemplate.query(ProductQueries.FIND_FOOD_COMPLEMENT, new ProductRowMapper());

	}
	
	/**
	 * findById finds all the results in the table based on the identifier field.
	 */
	public List<Products> findByName(String prodName) {
		return jdbcTemplate.query(ProductQueries.FIND_BY_NAME+prodName+"%'", new ProductRowMapper());
	}
	
	/**
	 * findById finds all the results in the table based on the identifier field.
	 */
	public Products findById(int id) {
		return jdbcTemplate.queryForObject(ProductQueries.FIND_BY_ID, new Object[] {id},
				new BeanPropertyRowMapper<Products>(Products.class));	
	}
	
	/**
	 * deleteById the request field in the table based on the identifier field.
	 */
	public int deleteById(int id) {
		return jdbcTemplate.update(ProductQueries.DELETE_BY_ID, new Object[] {id});	
	}
	
	/**
	 * insert Inserts a new field.
	 */
	public int insert(Products product) {
		return jdbcTemplate.update(ProductQueries.INSERT, new Object[] 
				{product.getIdProduct(),product.getProductName(),product.getProductType(),
					product.getSize(),product.getPrice()});	
	}
	
	/**
	 * update Updates a field.
	 */
	public int update(Products product) {
		return jdbcTemplate.update(ProductQueries.UPDATE, new Object[] 
						{product.getIdProduct(),product.getProductName(),product.getProductType(),
						product.getSize(),product.getPrice(),product.getIdProduct()});	
	}

}
