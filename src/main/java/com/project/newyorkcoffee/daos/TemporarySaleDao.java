package com.project.newyorkcoffee.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.project.newyorkcoffee.model.TemporarySale;
import com.project.newyorkcoffee.queries.TemporarySaleQueries;

@Repository
public class TemporarySaleDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	/**
	 * AdministrationRowMapper that works as a rowMaper to get all information related.
	 */
	class SaleRowMapper implements RowMapper<TemporarySale> {

		@Override
		public TemporarySale mapRow(ResultSet rs, int rowNum) throws SQLException {
			final TemporarySale tempSale = new TemporarySale();
			tempSale.setIdTempSale(rs.getInt(TemporarySaleQueries.ID_TEMP_SALE));
			tempSale.setDescription(rs.getString(TemporarySaleQueries.DESCRIPTION));
			tempSale.setQuantity(rs.getInt(TemporarySaleQueries.QUANTITY));
			tempSale.setTotal(rs.getDouble(TemporarySaleQueries.TOTAL));
			return tempSale;
		}	
	}
	
	/**
	 * findAll finds all the results in the table.
	 */
	public List<TemporarySale> findAll() {

		return jdbcTemplate.query(TemporarySaleQueries.FIND_ALL, new SaleRowMapper	());

	}
	
	/**
	 * findById finds all the results in the table based on the identifier field.
	 */
	public TemporarySale findById(int id) {
		return jdbcTemplate.queryForObject(TemporarySaleQueries.FIND_BY_ID, new Object[] {id},
				new BeanPropertyRowMapper<TemporarySale>(TemporarySale.class));	
	}
	
	
	/**
	 * deleteById the request field in the table based on the identifier field.
	 */
	public int deleteById(int id) {
		return jdbcTemplate.update(TemporarySaleQueries.DELETE_BY_ID, new Object[] {id});	
	}
	
	/**
	 * truncates the table temp sale.
	 */
	public int truncateTempSale() {
		return jdbcTemplate.update(TemporarySaleQueries.TRUNCATE_TEMP_SALE);	
	}
	
	/**
	 * insert Inserts a new field.
	 */
	public int insert(TemporarySale tempSale) {
		return jdbcTemplate.update(TemporarySaleQueries.INSERT, new Object[] 
				{tempSale.getIdTempSale(),tempSale.getDescription(),tempSale.getQuantity(),tempSale.getTotal()});	
	}
	
	/**
	 * update Updates a field.
	 */
	public int update(TemporarySale tempSale) {
		return jdbcTemplate.update(TemporarySaleQueries.UPDATE, new Object[] 
				{tempSale.getIdTempSale(),tempSale.getDescription(),tempSale.getQuantity(),
				tempSale.getTotal(),tempSale.getIdTempSale()});	
	}
}
