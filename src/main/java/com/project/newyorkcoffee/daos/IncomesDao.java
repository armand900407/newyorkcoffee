package com.project.newyorkcoffee.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.project.newyorkcoffee.model.Incomes;
import com.project.newyorkcoffee.queries.IncomesQueries;
import com.project.newyorkcoffee.queries.SalesQueries;

@Repository
public class IncomesDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	/**
	 * AdministrationRowMapper that works as a rowMaper to get all information related.
	 */
	class IncomesRowMapper implements RowMapper<Incomes> {

		@Override
		public Incomes mapRow(ResultSet rs, int rowNum) throws SQLException {
			final Incomes income = new Incomes();
			income.setIdIncome(rs.getInt(IncomesQueries.ID_INCOME));
			income.setNoSale(rs.getInt(IncomesQueries.NO_SALE));
			income.setDescription(rs.getString(IncomesQueries.DESCRIPTION));
			income.setPaymentType(rs.getString(IncomesQueries.PAYMENT_TYPE));
			income.setTotal(rs.getDouble(IncomesQueries.TOTAL));
			income.setIncomeDate(rs.getString(IncomesQueries.INCOME_DATE));
			income.setEmployee(rs.getString(IncomesQueries.EMPLOYEE));
			return income;
		}	
	}
	
	/**
	 * findAll finds all the results in the table.
	 */
	public List<Incomes> findAll() {
		return jdbcTemplate.query(IncomesQueries.FIND_ALL, new IncomesRowMapper	());
	}
	
	/**
	 * findAllCash finds all the results in the table.
	 */
	public double findAllCash() {
		return jdbcTemplate.queryForObject(IncomesQueries.FIND_TOTAL_CASH, Double.class);
	}
	
	/**
	 * findAllCash finds all the results in the table.
	 */
	public double findAllCard() {
		return jdbcTemplate.queryForObject(IncomesQueries.FIND_TOTAL_CARD,  Double.class);
	}
	
	/**
	 * findById finds all the results in the table based on the identifier field.
	 */
	public Incomes findById(int id) {
		return jdbcTemplate.queryForObject(IncomesQueries.FIND_BY_ID, new Object[] {id},
				new BeanPropertyRowMapper<Incomes>(Incomes.class));	
	}
	
	
	/**
	 * deleteById the request field in the table based on the identifier field.
	 */
	public int deleteById(int id) {
		return jdbcTemplate.update(IncomesQueries.DELETE_BY_ID, new Object[] {id});	
	}
	
	/**
	 * insert Inserts a new field.
	 */
	public int insert(Incomes income) {
		return jdbcTemplate.update(IncomesQueries.INSERT, new Object[] 
				{income.getIdIncome(),income.getNoSale(),income.getDescription(),income.getPaymentType(),income.getTotal(),
						income.getIncomeDate(),income.getEmployee()});	
	}
	
	/**
	 * update Updates a field.
	 */
	public int update(Incomes income) {
		return jdbcTemplate.update(IncomesQueries.UPDATE, new Object[] 
				{income.getIdIncome(),income.getNoSale(),income.getDescription(),income.getPaymentType(),income.getTotal(),
						income.getIncomeDate(),income.getEmployee(),income.getIdIncome()});
	}
	
	/**
	 * truncates the table incomes.
	 */
	public int truncateIncomes() {
		return jdbcTemplate.update(IncomesQueries.TRUNCATE_INCOMES);	
	}
}
