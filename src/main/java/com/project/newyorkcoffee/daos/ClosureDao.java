package com.project.newyorkcoffee.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.project.newyorkcoffee.model.Closure;
import com.project.newyorkcoffee.queries.ClosureQueries;

@Repository
public class ClosureDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	/**
	 * AdministrationRowMapper that works as a rowMaper to get all information related.
	 */
	class ClosureRowMapper implements RowMapper<Closure> {

		@Override
		public Closure mapRow(ResultSet rs, int rowNum) throws SQLException {
			final Closure closure = new Closure();
			closure.setIdClosure(rs.getInt(ClosureQueries.ID_CLOSURE));
			closure.setEmployeeName(rs.getString(ClosureQueries.EMPLOYEE_NAME));
			closure.setInitCash(rs.getDouble(ClosureQueries.INIT_CASH));
			closure.setTotalIn(rs.getDouble(ClosureQueries.TOTAL_IN));
			closure.setTotalOut(rs.getDouble(ClosureQueries.TOTAL_OUT));
			closure.setTotalCash(rs.getDouble(ClosureQueries.TOTAL_CASH));
			closure.setTotalCard(rs.getDouble(ClosureQueries.TOTAL_CARDS));
			closure.setTotalGenerated(rs.getDouble(ClosureQueries.TOTAL_GENERATED));
			closure.setClosureDate(rs.getString(ClosureQueries.CLOSURE_DATE));
			return closure;
		}	
	}
	
	/**
	 * findAll finds all the results in the table.
	 */
	public List<Closure> findAll() {

		return jdbcTemplate.query(ClosureQueries.FIND_ALL, new ClosureRowMapper	());

	}
	
	/**
	 * findById finds all the results in the table based on the identifier field.
	 */
	public Closure findById(int id) {
		return jdbcTemplate.queryForObject(ClosureQueries.FIND_BY_ID, new Object[] {id},
				new BeanPropertyRowMapper<Closure>(Closure.class));	
	}
	
	
	/**
	 * deleteById the request field in the table based on the identifier field.
	 */
	public int deleteById(int id) {
		return jdbcTemplate.update(ClosureQueries.DELETE_BY_ID, new Object[] {id});	
	}
	
	/**
	 * insert Inserts a new field.
	 */
	public int insert(Closure closure) {
		return jdbcTemplate.update(ClosureQueries.INSERT, new Object[] 
				{closure.getIdClosure(),closure.getEmployeeName(),closure.getInitCash(),closure.getTotalIn(),
				closure.getTotalOut(),closure.getTotalCash(),closure.getTotalCard(),closure.getTotalGenerated(),
				closure.getClosureDate()});	
	}
	
	/**
	 * update Updates a field.
	 */
	public int update(Closure closure) {
		return jdbcTemplate.update(ClosureQueries.UPDATE, new Object[] 
				{closure.getIdClosure(),closure.getEmployeeName(),closure.getInitCash(),closure.getTotalIn(),
						closure.getTotalOut(),closure.getTotalCash(),closure.getTotalCard(),closure.getTotalGenerated(),
						closure.getClosureDate(),closure.getIdClosure()});	
	}
}
