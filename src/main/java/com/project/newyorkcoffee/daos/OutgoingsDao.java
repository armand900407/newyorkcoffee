package com.project.newyorkcoffee.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.project.newyorkcoffee.model.Outgoings;
import com.project.newyorkcoffee.queries.IncomesQueries;
import com.project.newyorkcoffee.queries.OutgoingsQueries;

@Repository
public class OutgoingsDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	/**
	 * AdministrationRowMapper that works as a rowMaper to get all information related.
	 */
	class OutGoingsRowMapper implements RowMapper<Outgoings> {

		@Override
		public Outgoings mapRow(ResultSet rs, int rowNum) throws SQLException {
			final Outgoings outgoing = new Outgoings();
			outgoing.setIdOutgoing(rs.getInt(OutgoingsQueries.ID_OUTGOING));
			outgoing.setDescription(rs.getString(OutgoingsQueries.DESCRIPTION));
			outgoing.setPaymentType(rs.getString(OutgoingsQueries.PAYMENT_TYPE));
			outgoing.setTotal(rs.getDouble(OutgoingsQueries.TOTAL));
			outgoing.setOutgoingDate(rs.getString(OutgoingsQueries.OUTGOING_DATE));
			outgoing.setEmployee(rs.getString(OutgoingsQueries.EMPLOYEE));
			return outgoing;
		}	
	}
	
	/**
	 * findAll finds all the results in the table.
	 */
	public List<Outgoings> findAll() {
		return jdbcTemplate.query(OutgoingsQueries.FIND_ALL, new OutGoingsRowMapper	());
	}
	
	/**
	 * findAllCash finds all the results in the table.
	 */
	public double findAllCash() {
		return jdbcTemplate.queryForObject(OutgoingsQueries.FIND_TOTAL_CASH, Double.class);
	}
	
	/**
	 * findAllCash finds all the results in the table.
	 */
	public double findAllCard() {
		return jdbcTemplate.queryForObject(OutgoingsQueries.FIND_TOTAL_CARD,  Double.class);
	}
	
	/**
	 * findById finds all the results in the table based on the identifier field.
	 */
	public Outgoings findById(int id) {
		return jdbcTemplate.queryForObject(OutgoingsQueries.FIND_BY_ID, new Object[] {id},
				new BeanPropertyRowMapper<Outgoings>(Outgoings.class));	
	}
	
	
	/**
	 * deleteById the request field in the table based on the identifier field.
	 */
	public int deleteById(int id) {
		return jdbcTemplate.update(OutgoingsQueries.DELETE_BY_ID, new Object[] {id});	
	}
	
	/**
	 * insert Inserts a new field.
	 */
	public int insert(Outgoings outgoing) {
		return jdbcTemplate.update(OutgoingsQueries.INSERT, new Object[] 
				{outgoing.getIdOutgoing(),outgoing.getDescription(),outgoing.getPaymentType(), outgoing.getTotal(),
					outgoing.getOutgoingDate(),outgoing.getEmployee()});	
	}
	
	/**
	 * update Updates a field.
	 */
	public int update(Outgoings outgoing) {
		return jdbcTemplate.update(OutgoingsQueries.UPDATE, new Object[] 
						{outgoing.getIdOutgoing(),outgoing.getDescription(), outgoing.getPaymentType(), outgoing.getTotal(),
								outgoing.getOutgoingDate(),outgoing.getEmployee(),outgoing.getIdOutgoing()});
	}
	
	/**
	 * truncates the table Outgoings.
	 */
	public int truncateOutgoings() {
		return jdbcTemplate.update(OutgoingsQueries.TRUNCATE_OUTGOINGS);	
	}
}
