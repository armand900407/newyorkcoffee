package com.project.newyorkcoffee.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.project.newyorkcoffee.model.Sales;
import com.project.newyorkcoffee.queries.SalesQueries;
import com.project.newyorkcoffee.queries.TemporarySaleQueries;

@Repository
public class SalesDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	/**
	 * AdministrationRowMapper that works as a rowMaper to get all information related.
	 */
	class SaleRowMapper implements RowMapper<Sales> {

		@Override
		public Sales mapRow(ResultSet rs, int rowNum) throws SQLException {
			final Sales sale = new Sales();
			sale.setIdSale(rs.getInt(SalesQueries.ID_SALE));
			sale.setNoSale(rs.getInt(SalesQueries.NO_SALE));
			sale.setDescription(rs.getString(SalesQueries.DESCRIPTION));
			sale.setPaymentType(rs.getString(SalesQueries.PAYMENT_TYPE));
			sale.setTotal(rs.getDouble(SalesQueries.TOTAL));
			sale.setSaleDate(rs.getString(SalesQueries.SALE_DATE));
			return sale;
		}	
	}
	
	/**
	 * findAll finds all the results in the table.
	 */
	public List<Sales> findAll() {

		return jdbcTemplate.query(SalesQueries.FIND_ALL, new SaleRowMapper	());

	}
	
	/**
	 * findAll finds all the results in the table.
	 */
	public List<Sales> findByNoSale(int noSale) {

		return jdbcTemplate.query(SalesQueries.FIND_BY_NO_SALE+noSale, new SaleRowMapper	() );

	}
	
	/**
	 * findById finds all the results in the table based on the identifier field.
	 */
	public Sales findById(int id) {
		return (Sales) jdbcTemplate.query(SalesQueries.FIND_BY_ID, new Object[] {id},
				new BeanPropertyRowMapper<Sales>(Sales.class));	
	}
	
	
	/**
	 * deleteById the request field in the table based on the identifier field.
	 */
	public int deleteById(int id) {
		return jdbcTemplate.update(SalesQueries.DELETE_BY_ID, new Object[] {id});	
	}
	
	/**
	 * deleteById the request field in the table based on the sale number field.
	 */
	public int deleteByNoSale(int noSale) {
		return jdbcTemplate.update(SalesQueries.DELETE_BY_NO_SALE, new Object[] {noSale});	
	}
	
	/**
	 * insert Inserts a new field.
	 */
	public int insert(Sales sale) {
		return jdbcTemplate.update(SalesQueries.INSERT, new Object[] 
				{sale.getIdSale(),sale.getNoSale(),sale.getDescription(),sale.getPaymentType(),
				sale.getTotal(),sale.getSaleDate()});	
	}
	
	/**
	 * update Updates a field.
	 */
	public int update(Sales sale) {
		return jdbcTemplate.update(SalesQueries.UPDATE, new Object[] 
						{sale.getIdSale(),sale.getNoSale(),sale.getDescription(),sale.getPaymentType(),
								sale.getTotal(),sale.getSaleDate(),sale.getIdSale()});	
	}
	
	/**
	 * truncates the table sale.
	 */
	public int truncateSale() {
		return jdbcTemplate.update(SalesQueries.TRUNCATE_SALE);	
	}

}
