package com.project.newyorkcoffee.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.project.newyorkcoffee.model.Cash;
import com.project.newyorkcoffee.queries.CashQueries;

@Repository
public class CashDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	/**
	 * AdministrationRowMapper that works as a rowMaper to get all information related.
	 */
	class CashRowMapper implements RowMapper<Cash> {

		@Override
		public Cash mapRow(ResultSet rs, int rowNum) throws SQLException {
			final Cash cash = new Cash();
			cash.setIdCash(rs.getInt(CashQueries.ID_CASH));
			cash.setQuantity(rs.getDouble(CashQueries.QUANTITY));
			cash.setNoSale(rs.getInt(CashQueries.NO_SALE));
			return cash;
		}	
	}
	
	/**
	 * findAll finds all the results in the table.
	 */
	public List<Cash> findAll() {

		return jdbcTemplate.query(CashQueries.FIND_ALL, new CashRowMapper	());

	}
	
	/**
	 * findById finds all the results in the table based on the identifier field.
	 */
	public Cash findById(int id) {
		return jdbcTemplate.queryForObject(CashQueries.FIND_BY_ID, new Object[] {id},
				new BeanPropertyRowMapper<Cash>(Cash.class));	
	}
	
	
	/**
	 * deleteById the request field in the table based on the identifier field.
	 */
	public int deleteById(int id) {
		return jdbcTemplate.update(CashQueries.DELETE_BY_ID, new Object[] {id});	
	}
	
	/**
	 * insert Inserts a new field.
	 */
	public int insert(Cash cash) {
		return jdbcTemplate.update(CashQueries.INSERT, new Object[] 
				{cash.getIdCash(),cash.getQuantity(),cash.getNoSale()});	
	}
	
	/**
	 * update Updates a field.
	 */
	public int update(Cash cash) {
		return jdbcTemplate.update(CashQueries.UPDATE, new Object[] 
				{cash.getIdCash(),cash.getQuantity(),cash.getNoSale(),cash.getIdCash()});
	}
}
