package com.project.newyorkcoffee.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.project.newyorkcoffee.model.Administration;
import com.project.newyorkcoffee.queries.AdministrationQueries;

@Repository
public class AdministrationDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	/**
	 * AdministrationRowMapper that works as a rowMaper to get all information related.
	 */
	class AdministrationRowMapper implements RowMapper<Administration> {

		@Override
		public Administration mapRow(ResultSet rs, int rowNum) throws SQLException {
			final Administration adminstration = new Administration();
			adminstration.setIdEmployee(rs.getInt(AdministrationQueries.ID_EMPLOYEE));
			adminstration.setEmployeeType(rs.getString(AdministrationQueries.EMPLOYEE_TYPE));
			adminstration.setEmployeeName(rs.getString(AdministrationQueries.EMPLOYEE_NAME));
			adminstration.setEmployeeLastName(rs.getString(AdministrationQueries.EMPLOYEE_LAST_NAME));
			adminstration.setPhone(rs.getString(AdministrationQueries.EMPLOYEE_PHONE));
			adminstration.setPass(rs.getString(AdministrationQueries.PASSWORD));
			return adminstration;
		}

		
	}
	
	/**
	 * findAll finds all the results in the table.
	 */
	public List<Administration> findAll() {

		return jdbcTemplate.query(AdministrationQueries.FIND_ALL, new AdministrationRowMapper());

	}
	
	/**
	 * findById finds all the results in the table based on the identifier field.
	 */
	public Administration findById(int id) {
		return jdbcTemplate.queryForObject(AdministrationQueries.FIND_BY_ID, new Object[] {id},
				new BeanPropertyRowMapper<Administration>(Administration.class));	
	}
	
	/**
	 * findById finds all the results in the table based on the identifier field.
	 */
	public Administration findByAuthentication(String user,String pass) {
		return jdbcTemplate.queryForObject(AdministrationQueries.FIND_BY_AUTHENTICATION, new Object[] {user,pass},
				new BeanPropertyRowMapper<Administration>(Administration.class));	
	}
	
	/**
	 * deleteById the request field in the table based on the identifier field.
	 */
	public int deleteById(int id) {
		return jdbcTemplate.update(AdministrationQueries.DELETE_BY_ID, new Object[] {id});	
	}
	
	/**
	 * insert Inserts a new field.
	 */
	public int insert(Administration administration) {
		return jdbcTemplate.update(AdministrationQueries.INSERT, new Object[] 
				{administration.getIdEmployee(),administration.getEmployeeType(),administration.getEmployeeName(),
				administration.getEmployeeLastName(),administration.getPhone(),administration.getPass()});	
	}
	
	/**
	 * update Updates a field.
	 */
	public int update(Administration administration) {
		return jdbcTemplate.update(AdministrationQueries.UPDATE, new Object[] 
						{administration.getIdEmployee(),administration.getEmployeeType(),administration.getEmployeeName(),
						administration.getEmployeeLastName(),administration.getPhone(),administration.getPass(),administration.getIdEmployee()});
	}
	
}
