package com.project.newyorkcoffee.model;

import java.io.Serializable;
import java.util.Objects;

public class Products implements Serializable{

	/**
	 * Serial Version.
	 */
	private static final long serialVersionUID = 7785158650075642366L;
	/**
	 * idProduct that corresponds to the idProduct.
	 */
	private Integer idProduct;
	/**
	 * productName that corresponds to the productName.
	 */
	private String productName;
	/**
	 * productType that corresponds to the productType.
	 */
	private String productType;
	/**
	 * size that corresponds to the product size.
	 */
	private String size;
	/**
	 * price that corresponds to the price.
	 */
	private Double price;
	/**
	 * Constructor with Arguments for Administration.
	 */
	public Products(Integer idProduct, String productName, String productType, String size, Double price) {
		this.idProduct = idProduct;
		this.productName = productName;
		this.productType = productType;
		this.size = size;
		this.price = price;
	}
	/**
	 * Constructor with No Arguments.
	 */
	public Products() {
	}
	
	/**
	 * hashCode.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(idProduct, productName, productType, size, price);
	}
	
	/**
	 * equals.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Products other = (Products) obj;
		return Objects.equals(idProduct, other.idProduct) && Objects.equals(productName, other.productName) 
				&& Objects.equals(productType, other.productType) && Objects.equals(size, other.size)
				&& Objects.equals(price, other.price);
	}
	
	/**
	 * toString.
	 */
	@Override
	public String toString() {
		return "Products [idProduct=" + idProduct + ", productName=" + productName + ", productType=" + productType
				+ ", size=" + size + ", price=" + price + "]";
	}
	public Integer getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
}
