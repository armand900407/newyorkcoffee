package com.project.newyorkcoffee.model;

import java.io.Serializable;
import java.util.Objects;

public class Closure implements Serializable {

	/**
	 * Serial Version.
	 */
	private static final long serialVersionUID = 4664761881145239175L;
	/**
	 * idClosure that corresponds to the idClosure.
	 */
	private Integer idClosure;
	/**
	 * employeeName that corresponds to the employeeName.
	 */
	private String employeeName;
	/**
	 * initCash that corresponds to the initCash.
	 */
	private Double initCash;
	/**
	 * totalIn that corresponds to the totalIn.
	 */
	private Double totalIn;
	/**
	 * totalOut that corresponds to totalOut.
	 */
	private Double totalOut;
	/**
	 * totalCash that corresponds to the totalCash.
	 */
	private Double totalCash;
	/**
	 * totalCard that corresponds to totalCard.
	 */
	private Double totalCard;
	/**
	 * totalGenerated that corresponds to the totalGenerated.
	 */
	private Double totalGenerated;
	/**
	 * closureDate that corresponds to the closureDate.
	 */
	private String closureDate;
	
	/**
	 * Constructor with Arguments for Closure.
	 */
	public Closure(Integer idClosure, String employeeName, Double initCash, Double totalIn, Double totalOut,
			Double totalCash, Double totalCard, Double totalGenerated, String closureDate) {
		this.idClosure = idClosure;
		this.employeeName = employeeName;
		this.initCash = initCash;
		this.totalIn = totalIn;
		this.totalOut = totalOut;
		this.totalCash = totalCash;
		this.totalCard = totalCard;
		this.totalGenerated = totalGenerated;
		this.closureDate = closureDate;
	}
	/**
	 * Constructor with No Arguments for Closure.
	 */
	public Closure() {
	}
	
	/**
	 * hashCode.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(idClosure, employeeName, initCash, totalIn, totalOut, totalCash, 
				totalCard, totalGenerated,closureDate);
	}
	
	/**
	 * equals.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Closure other = (Closure) obj;
		return Objects.equals(idClosure, other.idClosure) && Objects.equals(employeeName, other.employeeName) 
				&& Objects.equals(initCash, other.initCash) && Objects.equals(totalIn, other.totalIn)
				&& Objects.equals(totalOut, other.totalOut) && Objects.equals(totalCash, other.totalCash)
				&& Objects.equals(totalCard, other.totalCard)&& Objects.equals(totalGenerated, other.totalGenerated)
				&& Objects.equals(closureDate, other.closureDate);
	}
	
	/**
	 * toString.
	 */
	@Override
	public String toString() {
		return "Closure [idClosure=" + idClosure + ", employeeName=" + employeeName + ", initCash=" + initCash
				+ ", totalIn=" + totalIn + ", totalOut=" + totalOut + ", totalCash=" + totalCash + ", totalCard="
				+ totalCard + ", totalGenerated=" + totalGenerated + ", closureDate=" + closureDate + "]";
	}
	public Integer getIdClosure() {
		return idClosure;
	}
	public void setIdClosure(Integer idClosure) {
		this.idClosure = idClosure;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public Double getInitCash() {
		return initCash;
	}
	public void setInitCash(Double initCash) {
		this.initCash = initCash;
	}
	public Double getTotalIn() {
		return totalIn;
	}
	public void setTotalIn(Double totalIn) {
		this.totalIn = totalIn;
	}
	public Double getTotalOut() {
		return totalOut;
	}
	public void setTotalOut(Double totalOut) {
		this.totalOut = totalOut;
	}
	public Double getTotalCash() {
		return totalCash;
	}
	public void setTotalCash(Double totalCash) {
		this.totalCash = totalCash;
	}
	public Double getTotalCard() {
		return totalCard;
	}
	public void setTotalCard(Double totalCard) {
		this.totalCard = totalCard;
	}
	public Double getTotalGenerated() {
		return totalGenerated;
	}
	public void setTotalGenerated(Double totalGenerated) {
		this.totalGenerated = totalGenerated;
	}
	public String getClosureDate() {
		return closureDate;
	}
	public void setClosureDate(String closureDate) {
		this.closureDate = closureDate;
	}
	
}
