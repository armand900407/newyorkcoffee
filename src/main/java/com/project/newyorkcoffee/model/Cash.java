package com.project.newyorkcoffee.model;

import java.io.Serializable;
import java.util.Objects;

public class Cash implements Serializable {

	/**
	 * Serial Version.
	 */
	private static final long serialVersionUID = 2217426764388297066L;
	
	/**
	 * idCash that corresponds to the idCash.
	 */
	private Integer idCash;
	/**
	 * quantity that corresponds to the product description bought.
	 */
	private Double quantity;
	/**
	 * noSale that corresponds to the noSale.
	 */
	private Integer noSale;

	/**
	 * Constructor with Arguments for Outgoings.
	 */
	public Cash(Integer idCash, Double quantity, Integer noSale) {
		this.idCash = idCash;
		this.quantity = quantity;
		this.noSale = noSale;
	}
	
	/**
	 * Constructor with No Arguments for Cash.
	 */
	public Cash() {
	}

	/**
	 * hashCode.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(idCash, quantity, noSale);
	}

	/**
	 * equals.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cash other = (Cash) obj;
		return Objects.equals(idCash, other.idCash) && Objects.equals(quantity, other.quantity) 
				&& Objects.equals(noSale, other.noSale);
	}

	/**
	 * toString.
	 */
	@Override
	public String toString() {
		return "Cash [idCash=" + idCash + ", quantity=" + quantity + ", noSale=" + noSale + "]";
	}

	public Integer getIdCash() {
		return idCash;
	}

	public void setIdCash(Integer idCash) {
		this.idCash = idCash;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Integer getNoSale() {
		return noSale;
	}

	public void setNoSale(Integer noSale) {
		this.noSale = noSale;
	}

}
