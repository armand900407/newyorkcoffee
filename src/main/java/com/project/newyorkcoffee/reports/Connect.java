package com.project.newyorkcoffee.reports;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class Connect {
	
	Connection connect = null;
    
    
    public Connection conexion(){
        try{
            Class.forName("org.gjt.mm.mysql.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://localhost:33061/nycoffee","root","mysql");
            //JOptionPane.showMessageDialog(null,"Conectado a base de datos NewYork Coffee");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Error"+ e);
        }
        return connect;
    }
    
    public void desconectar(){
    connect = null;
    //System.out.println("Desconexion a base de datos listo...");
    }
    
    public static void main(String[] args) {
    	Connect o = new Connect();
        o.conexion();
    }

}
