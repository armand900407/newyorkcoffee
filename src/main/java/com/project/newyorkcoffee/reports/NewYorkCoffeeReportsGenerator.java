package com.project.newyorkcoffee.reports;

import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

public class NewYorkCoffeeReportsGenerator {
	
	//: SALES ://
	private static final String PATH_SALES = "src/main/resources/com/project/newyorkcoffee/jasper/reports/salesdata.jasper";
	private static final String PDF_SALES ="/home/armando/Documents/NewYorkCoffeeReports/ReporteVentas.pdf";
	// "C:\\NewYorkCoffeeReports\\ReporteVentas.pdf";
	//: INCOMES ://
	private static final String PATH_INCOMES = "src/main/resources/com/project/newyorkcoffee/jasper/reports/incomes.jasper";
	private static final String PDF_INCOMES ="/home/armando/Documents/NewYorkCoffeeReports/ReporteEntradas.pdf";
	// "C:\\NewYorkCoffeeReports\\ReporteEntradas.pdf";
	//: OUTGONINGS ://
	private static final String PATH_OUTGOINGS = "src/main/resources/com/project/newyorkcoffee/jasper/reports/outgoings.jasper";
	private static final String PDF_OUTGOINGS ="/home/armando/Documents/NewYorkCoffeeReports/ReporteSalidas.pdf";
	// "C:\\NewYorkCoffeeReports\\ReporteCorte.pdf";
	//: CLOSURE ://
	private static final String PATH_CLOSURE = "src/main/resources/com/project/newyorkcoffee/jasper/reports/closure.jasper";
	private static final String PDF_CLOSURE ="/home/armando/Documents/NewYorkCoffeeReports/ReporteCorte.pdf";
	// "C:\\NewYorkCoffeeReports\\ReporteCorte.pdf";
	
	private static final String REPORT_GENERATION_ERROR ="Asegurese que exista la carpeta  C:\\NewYorkCoffeeReports\\";
	
	public static void salesReport() throws SQLException{
        
		Connect cn = new Connect(); 
	    JasperReport jr = null;
	       try{
	       jr = (JasperReport) JRLoader.loadObjectFromFile(PATH_SALES);
	       JasperPrint jp = JasperFillManager.fillReport(jr, null,cn.conexion());
	       JRExporter exp = new JRPdfExporter();
	       exp.setParameter(JRExporterParameter.JASPER_PRINT, jp);
	       exp.setParameter(JRExporterParameter.OUTPUT_FILE, new java.io.File(PDF_SALES));
	       exp.exportReport();
	       }catch(JRException ex){
	           java.util.logging.Logger.getLogger(NewYorkCoffeeReportsGenerator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	           JOptionPane.showMessageDialog(null,REPORT_GENERATION_ERROR);
	       } 
	    }   
	
	public static void incomesReport() throws SQLException{
        
		Connect cn = new Connect(); 
	    JasperReport jr = null;
	       try{
	       jr = (JasperReport) JRLoader.loadObjectFromFile(PATH_INCOMES);
	       JasperPrint jp = JasperFillManager.fillReport(jr, null,cn.conexion());
	       JRExporter exp = new JRPdfExporter();
	       exp.setParameter(JRExporterParameter.JASPER_PRINT, jp);
	       exp.setParameter(JRExporterParameter.OUTPUT_FILE, new java.io.File(PDF_INCOMES));
	       exp.exportReport();
	       }catch(JRException ex){
	           java.util.logging.Logger.getLogger(NewYorkCoffeeReportsGenerator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	           JOptionPane.showMessageDialog(null,REPORT_GENERATION_ERROR);
	       } 
	    } 
	
	public static void outgoingsReport() throws SQLException{
		Connect cn = new Connect(); 
	    JasperReport jr = null;
	       try{
	       jr = (JasperReport) JRLoader.loadObjectFromFile(PATH_OUTGOINGS);
	       JasperPrint jp = JasperFillManager.fillReport(jr, null,cn.conexion());
	       JRExporter exp = new JRPdfExporter();
	       exp.setParameter(JRExporterParameter.JASPER_PRINT, jp);
	       exp.setParameter(JRExporterParameter.OUTPUT_FILE, new java.io.File(PDF_OUTGOINGS));
	       exp.exportReport();
	       }catch(JRException ex){
	           java.util.logging.Logger.getLogger(NewYorkCoffeeReportsGenerator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	          JOptionPane.showMessageDialog(null,REPORT_GENERATION_ERROR);
	       } 

	    } 
	
	public static void closureReport() throws SQLException{
		Connect cn = new Connect(); 
	    JasperReport jr = null;
	       try{
	       jr = (JasperReport) JRLoader.loadObjectFromFile(PATH_CLOSURE);
	       JasperPrint jp = JasperFillManager.fillReport(jr, null,cn.conexion());
	       JRExporter exp = new JRPdfExporter();
	       exp.setParameter(JRExporterParameter.JASPER_PRINT, jp);
	       exp.setParameter(JRExporterParameter.OUTPUT_FILE, new java.io.File(PDF_CLOSURE));
	       exp.exportReport();
	       }catch(JRException ex){
	           java.util.logging.Logger.getLogger(NewYorkCoffeeReportsGenerator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	           JOptionPane.showMessageDialog(null,REPORT_GENERATION_ERROR);
	       } 

	    } 
	
	
	public static void main(String []args) throws SQLException{
		NewYorkCoffeeReportsGenerator r = new NewYorkCoffeeReportsGenerator();
		//r.closureReport();
	    // r.outgoingsReport();
	    // r.incomesReport();
		//r.salesReport();
	} 

}
