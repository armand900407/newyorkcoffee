package com.project.newyorkcoffee.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.newyorkcoffee.daos.SalesDao;
import com.project.newyorkcoffee.model.Sales;
import com.project.newyorkcoffee.reports.NewYorkCoffeeReportsGenerator;

@RestController
@RequestMapping("newyorkcoffee/v1/sales")
public class SalesController {
	@Autowired
	private SalesDao salesDao;

	@GetMapping("/getAll")
	public List<Sales> retrieveAllSales(){
	 return salesDao.findAll();
	}
	
	@GetMapping("/getSale/{id}")
	public Sales retrieveSale(@PathVariable int id){
	 return salesDao.findById(id);
	}
	
	@GetMapping("/getNoSale/{noSale}")
	public List<Sales> retrieveSalebyNoSale(@PathVariable int noSale) throws SQLException{
	 return salesDao.findByNoSale(noSale);
	}
	
	@DeleteMapping("/deleteSale/{id}")
	public int deleteSale(@PathVariable int id){
	 return salesDao.deleteById(id);
	}
	
	@DeleteMapping("/deleteNoSale/{noSale}")
	public int deleteByNoSale(@PathVariable int noSale){
	 return salesDao.deleteByNoSale(noSale);
	}
	
	@PostMapping("/createSale")
	public int createSale(@RequestBody Sales sale){
	 return salesDao.insert(sale);
	}

	@PutMapping("/updateProd")
	public int updateSale(@RequestBody Sales sale){
	 return salesDao.update(sale);
	}
	
	@GetMapping("/truncateSales")
	public void truncateSale(){
		salesDao.truncateSale();
	}
}
