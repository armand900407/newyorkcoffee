package com.project.newyorkcoffee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.newyorkcoffee.daos.AdministrationDao;
import com.project.newyorkcoffee.model.Administration;

@RestController
@RequestMapping("newyorkcoffee/v1/admin")
public class AdministratorController {

	@Autowired
	private AdministrationDao admDao;

	@GetMapping("/getAll")
	public List<Administration> retrieveAllAdministrators(){
	 return admDao.findAll();
	}
	
	@GetMapping("/getAdm/{id}")
	public Administration retrieveAdministrator(@PathVariable int id){
	 return admDao.findById(id);
	}
	
	@GetMapping("/getAdmAuth/{user}/{pass}")
	public Administration retrieveAdministrator(@PathVariable String user,@PathVariable String pass){
	 return admDao.findByAuthentication(user,pass);
	}
	
	@DeleteMapping("/deleteAdm/{id}")
	public int deleteAdministrator(@PathVariable int id){
	 return admDao.deleteById(id);
	}
	
	@PostMapping("/createAdm")
	public int createAdministrator(@RequestBody Administration administration){
	 return admDao.insert(administration);
	}

	@PutMapping("/updateAdm")
	public int updateAdministrator(@RequestBody Administration administration){
	 return admDao.update(administration);
	}
	
}
