package com.project.newyorkcoffee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.newyorkcoffee.daos.IncomesDao;
import com.project.newyorkcoffee.model.Incomes;

@RestController
@RequestMapping("newyorkcoffee/v1/incomes")
public class IncomesController {
	@Autowired
	private IncomesDao incomesDao;

	@GetMapping("/getAll")
	public List<Incomes> retrieveAllIncomes(){
	 return incomesDao.findAll();
	}
	
	@GetMapping("/getAllCash")
	public double retrieveAllIncomesCash(){
	 return incomesDao.findAllCash();
	}
	
	@GetMapping("/getAllCard")
	public double retrieveAllIncomesCard(){
	 return incomesDao.findAllCard();
	}
	
	@GetMapping("/getIncome/{id}")
	public Incomes retrieveIncome(@PathVariable int id){
	 return incomesDao.findById(id);
	}
	
	@DeleteMapping("/deleteIncome/{id}")
	public int deleteIncome(@PathVariable int id){
	 return incomesDao.deleteById(id);
	}
	
	@PostMapping("/createIncome")
	public int createIncome(@RequestBody Incomes incomes){
	 return incomesDao.insert(incomes);
	}

	@PutMapping("/updateIncome")
	public int updateIncome(@RequestBody Incomes incomes){
	 return incomesDao.update(incomes);
	}
	
	@GetMapping("/truncateIncomes")
	public void truncateIncomes(){
		incomesDao.truncateIncomes();
	}
}
