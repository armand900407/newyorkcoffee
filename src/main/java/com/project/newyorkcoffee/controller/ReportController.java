package com.project.newyorkcoffee.controller;

import java.sql.SQLException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.project.newyorkcoffee.reports.NewYorkCoffeeReportsGenerator;

@RestController
@RequestMapping("newyorkcoffee/v1/report")
public class ReportController {
	
	@GetMapping("/generateAll")
	public String retrieveAllReports(){
		String response="REPORTS GENERATED SUCCESSFULLY!";
		try {
			NewYorkCoffeeReportsGenerator.closureReport();
			NewYorkCoffeeReportsGenerator.incomesReport();
			NewYorkCoffeeReportsGenerator.outgoingsReport();
			NewYorkCoffeeReportsGenerator.salesReport();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response="REPORTS GENERATION FAILED!";
		}
		
	 return response;
	}

}
