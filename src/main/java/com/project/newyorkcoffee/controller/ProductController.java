package com.project.newyorkcoffee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.newyorkcoffee.daos.ProductDao;
import com.project.newyorkcoffee.model.Products;

@RestController
@RequestMapping("newyorkcoffee/v1/product")
public class ProductController {
	@Autowired
	private ProductDao productDao;

	@GetMapping("/getAll")
	public List<Products> retrieveAllProducts(){
	 return productDao.findAll();
	}
	
	@GetMapping("/getHotDrinks")
	public List<Products> retrieveHotDrinks(){
	 return productDao.findHotDrinks();
	}
	
	@GetMapping("/getColdDrinks")
	public List<Products> retrieveColdDrinks(){
	 return productDao.findColdDrinks();
	}
	
	@GetMapping("/getCocktailDrinks")
	public List<Products> retrieveCocktailDrinks(){
	 return productDao.findCocktailDrinks();
	}
	
	@GetMapping("/getFood")
	public List<Products> retrieveFood(){
	 return productDao.findFood();
	}
	
	@GetMapping("/getDrinkComplement")
	public List<Products> retrieveDrinkComplement(){
	 return productDao.findDrinkComplement();
	}
	
	@GetMapping("/getFoodComplement")
	public List<Products> retrieveFoodComplement(){
	 return productDao.findFoodComplement();
	}
	
	@GetMapping("/getProdByName/{prodName}")
	public List<Products> retrieveProductByName(@PathVariable String prodName){
	 return productDao.findByName(prodName);
	}
	
	@GetMapping("/getProd/{id}")
	public Products retrieveProduct(@PathVariable int id){
	 return productDao.findById(id);
	}
	
	@DeleteMapping("/deleteProd/{id}")
	public int deleteProduct(@PathVariable int id){
	 return productDao.deleteById(id);
	}
	
	@PostMapping("/createProd")
	public int createProduct(@RequestBody Products product){
		System.out.println("Before Inserting Product: "+product.toString());
	 return productDao.insert(product);
	}

	@PutMapping("/updateProd")
	public int updateProduct(@RequestBody Products product){
	 return productDao.update(product);
	}
}
