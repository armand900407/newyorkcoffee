package com.project.newyorkcoffee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.newyorkcoffee.daos.ClosureDao;
import com.project.newyorkcoffee.model.Closure;

@RestController
@RequestMapping("newyorkcoffee/v1/closure")
public class ClosureController {
	@Autowired
	private ClosureDao closureDao;

	@GetMapping("/getAll")
	public List<Closure> retrieveAllClosures(){
	 return closureDao.findAll();
	}
	
	@GetMapping("/getClosure/{id}")
	public Closure retrieveClosure(@PathVariable int id){
	 return closureDao.findById(id);
	}
	
	@DeleteMapping("/deleteClosure/{id}")
	public int deleteClosure(@PathVariable int id){
	 return closureDao.deleteById(id);
	}
	
	@PostMapping("/createClosure")
	public int createClosure(@RequestBody Closure closure){
	 return closureDao.insert(closure);
	}

	@PutMapping("/updateClosure")
	public int updateClosure(@RequestBody Closure closure){
	 return closureDao.update(closure);
	}
}
