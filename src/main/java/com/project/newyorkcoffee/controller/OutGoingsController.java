package com.project.newyorkcoffee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.newyorkcoffee.daos.OutgoingsDao;
import com.project.newyorkcoffee.model.Outgoings;

@RestController
@RequestMapping("newyorkcoffee/v1/outgoings")
public class OutGoingsController {
	@Autowired
	private OutgoingsDao outgoingsDao;

	@GetMapping("/getAll")
	public List<Outgoings> retrieveAllOutgoings(){
	 return outgoingsDao.findAll();
	}
	
	@GetMapping("/getAllCash")
	public double retrieveAllOutgoingsCash(){
	 return outgoingsDao.findAllCash();
	}
	
	@GetMapping("/getAllCard")
	public double retrieveAllOutgoingsCard(){
	 return outgoingsDao.findAllCard();
	}
	
	@GetMapping("/getOutgoing/{id}")
	public Outgoings retrieveOutgoing(@PathVariable int id){
	 return outgoingsDao.findById(id);
	}
	
	@DeleteMapping("/deleteOutgoing/{id}")
	public int deleteOutgoing(@PathVariable int id){
	 return outgoingsDao.deleteById(id);
	}
	
	@PostMapping("/createOutgoing")
	public int createOutgoing(@RequestBody Outgoings outgoing){
	 return outgoingsDao.insert(outgoing);
	}

	@PutMapping("/updateOutgoings")
	public int updateOutgoing(@RequestBody Outgoings outgoing){
	 return outgoingsDao.update(outgoing);
	}
	
	@GetMapping("/truncateOutgoings")
	public void truncateOutgoings(){
		outgoingsDao.truncateOutgoings();
	}
}
