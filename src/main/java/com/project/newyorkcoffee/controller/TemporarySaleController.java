package com.project.newyorkcoffee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.newyorkcoffee.daos.TemporarySaleDao;
import com.project.newyorkcoffee.model.TemporarySale;

@RestController
@RequestMapping("newyorkcoffee/v1/tempsale")
public class TemporarySaleController {
	@Autowired
	private TemporarySaleDao tempSalesDao;

	@GetMapping("/getAll")
	public List<TemporarySale> retrieveAllSales(){
	 return tempSalesDao.findAll();
	}
	
	@GetMapping("/getTempSale/{id}")
	public TemporarySale retrieveSale(@PathVariable int id){
	 return tempSalesDao.findById(id);
	}
	
	@GetMapping("/truncateTempSale")
	public void truncateTempSale(){
		tempSalesDao.truncateTempSale();
	}
	
	@DeleteMapping("/deleteTempSale/{id}")
	public int deleteSale(@PathVariable int id){
	 return tempSalesDao.deleteById(id);
	}
	
	@PostMapping("/createTempSale")
	public int createSale(@RequestBody TemporarySale sale){
	 return tempSalesDao.insert(sale);
	}

	@PutMapping("/updateTempSale")
	public int updateSale(@RequestBody TemporarySale sale){
	 return tempSalesDao.update(sale);
	}
}
