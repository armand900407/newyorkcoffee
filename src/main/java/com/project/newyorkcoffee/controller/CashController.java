package com.project.newyorkcoffee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.newyorkcoffee.daos.CashDao;
import com.project.newyorkcoffee.model.Cash;


@RestController
@RequestMapping("newyorkcoffee/v1/cash")
public class CashController {
	@Autowired
	private CashDao cashDao;

	@GetMapping("/getAll")
	public List<Cash> retrieveAllCash(){
	 return cashDao.findAll();
	}
	
	@GetMapping("/getCash/{id}")
	public Cash retrieveCash(@PathVariable int id){
	 return cashDao.findById(id);
	}
	
	@DeleteMapping("/deleteCash/{id}")
	public int deleteCash(@PathVariable int id){
	 return cashDao.deleteById(id);
	}
	
	@PostMapping("/createCash")
	public int createCash(@RequestBody Cash cash){
	 return cashDao.insert(cash);
	}

	@PutMapping("/updateCash")
	public int updateCash(@RequestBody Cash cash){
	 return cashDao.update(cash);
	}
}
