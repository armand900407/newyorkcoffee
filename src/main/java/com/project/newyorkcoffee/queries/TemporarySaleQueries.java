package com.project.newyorkcoffee.queries;

public class TemporarySaleQueries {

	public static final String ID_TEMP_SALE ="id_tempSale";
	
	public static final String DESCRIPTION ="description";
	
	public static final String QUANTITY ="quantity";
	
	public static final String TOTAL ="total";
	
	public static final String FIND_ALL ="SELECT * FROM nycoffee.temp_sale";
	
	public static final String FIND_BY_ID ="SELECT * FROM nycoffee.temp_sale WHERE id_tempSale=?";
	
	public static final String DELETE_BY_ID ="DELETE FROM nycoffee.temp_sale WHERE id_tempSale=?";
	
	public static final String TRUNCATE_TEMP_SALE ="TRUNCATE nycoffee.temp_sale";
	
	public static final String INSERT ="INSERT INTO nycoffee.temp_sale (id_tempSale, description, quantity, total) VALUES (?,?,?,?)";
	
	public static final String UPDATE ="UPDATE nycoffee.temp_sale SET id_tempSale=?, description=?, quantity=?, total=? WHERE id_tempSale=?";

}
