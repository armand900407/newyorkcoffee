package com.project.newyorkcoffee.queries;


public class SalesQueries {

	public static final String ID_SALE ="id_sale";
	
	public static final String NO_SALE ="no_sale";
	
	public static final String DESCRIPTION ="description";
	
	public static final String PAYMENT_TYPE ="payment_type";
	
	public static final String TOTAL ="total";
	
	public static final String SALE_DATE ="sale_date";
	
	public static final String FIND_ALL ="SELECT * FROM nycoffee.sales";
	
	public static final String FIND_BY_ID ="SELECT * FROM nycoffee.sales WHERE id_sale=?";
	
	public static final String FIND_BY_NO_SALE ="SELECT * FROM nycoffee.sales WHERE no_sale=";
	
	public static final String DELETE_BY_ID ="DELETE FROM nycoffee.sales WHERE id_sale=?";
	
	public static final String DELETE_BY_NO_SALE ="DELETE FROM nycoffee.sales WHERE no_sale=?";
	
	public static final String INSERT ="INSERT INTO nycoffee.sales (id_sale, no_sale, description, payment_type, total, sale_date) VALUES (?,?,?,?,?,?)";
	
	public static final String UPDATE ="UPDATE nycoffee.sales SET id_sale=?, no_sale=?, description=?, payment_type=?, total=?, sale_date=? WHERE id_sale=?";

	public static final String TRUNCATE_SALE ="TRUNCATE nycoffee.sales";
	
}
