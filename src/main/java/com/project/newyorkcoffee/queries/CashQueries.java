package com.project.newyorkcoffee.queries;

public class CashQueries {
	
	public static final String ID_CASH="id_cash";
	
	public static final String QUANTITY="quantity";
	
	public static final String NO_SALE="no_sale";
	
	public static final String FIND_ALL ="SELECT * FROM nycoffee.cash";
	
	public static final String FIND_BY_ID ="SELECT * FROM nycoffee.cash WHERE id_cash=?";
	
	public static final String DELETE_BY_ID ="DELETE FROM nycoffee.cash WHERE id_cash=?";
	
	public static final String INSERT ="INSERT INTO nycoffee.cash (id_cash,quantity,no_sale) VALUES (?,?,?)";
	
	public static final String UPDATE ="UPDATE nycoffee.cash SET id_cash=?, quantity=?, no_sale=? WHERE id_cash=?";
}
