package com.project.newyorkcoffee.queries;

public class OutgoingsQueries {

	public static final String ID_OUTGOING ="id_outgoing";
	
	public static final String DESCRIPTION ="description";
	
	public static final String PAYMENT_TYPE ="payment_type";
	
	public static final String TOTAL ="total";
	
	public static final String OUTGOING_DATE ="outgoingDate";
	
	public static final String EMPLOYEE ="employee";
	
	public static final String FIND_ALL ="SELECT * FROM nycoffee.outgoings";
	
	public static final String FIND_TOTAL_CASH ="SELECT SUM(TOTAL) AS TOTAL FROM nycoffee.outgoings WHERE payment_type = 'Efectivo'";
	
	public static final String FIND_TOTAL_CARD ="SELECT SUM(TOTAL) AS TOTAL FROM nycoffee.outgoings WHERE payment_type = 'Tarjeta'";
	
	public static final String FIND_BY_ID ="SELECT * FROM nycoffee.outgoings WHERE id_outgoing=?";
	
	public static final String DELETE_BY_ID ="DELETE FROM nycoffee.outgoings WHERE id_outgoing=?";
	
	public static final String INSERT ="INSERT INTO nycoffee.outgoings (id_outgoing,description,payment_type,total,outgoingDate,employee) VALUES (?,?,?,?,?,?)";
	
	public static final String UPDATE ="UPDATE nycoffee.outgoings SET id_outgoing=?, description=?, payment_type=?, total=?, outgoingDate=?, employee=? WHERE id_outgoing=?";
	
	public static final String TRUNCATE_OUTGOINGS ="TRUNCATE nycoffee.outgoings";
	
}
