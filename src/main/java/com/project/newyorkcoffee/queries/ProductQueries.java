package com.project.newyorkcoffee.queries;

import com.project.newyorkcoffee.catalogs.ProductTypesCatalog;

public class ProductQueries {

	public static final String ID_PRODUCT ="id_product";
	
	public static final String PRODUCT_TYPE ="product_type";
	
	public static final String PRODUCT_NAME ="product_name";
	
	public static final String SIZE ="size";
	
	public static final String PRICE ="price";
	
	public static final String FIND_ALL ="SELECT * FROM nycoffee.products";
	
	public static final String FIND_HOT_DRINKS ="SELECT * FROM nycoffee.products WHERE product_type='"+ProductTypesCatalog.HOT_DRINK+"'";
	
	public static final String FIND_COLD_DRINKS ="SELECT * FROM nycoffee.products WHERE product_type='"+ProductTypesCatalog.COLD_DRINK+"'";
	
	public static final String FIND_COCKTAIL_DRINKS ="SELECT * FROM nycoffee.products WHERE product_type='"+ProductTypesCatalog.COCKTAIL_DRINK+"'";

	public static final String FIND_FOOD ="SELECT * FROM nycoffee.products WHERE product_type='"+ProductTypesCatalog.FOOD+"'";
	
	public static final String FIND_DRINK_COMPLEMENT ="SELECT * FROM nycoffee.products WHERE product_type='"+ProductTypesCatalog.DRINK_COMPLEMENT+"'";
	
	public static final String FIND_FOOD_COMPLEMENT ="SELECT * FROM nycoffee.products WHERE product_type='"+ProductTypesCatalog.FOOD_COMPLEMENT+"'";
	
	public static final String FIND_BY_ID ="SELECT * FROM nycoffee.products WHERE id_product=?";
	
	public static final String FIND_BY_NAME ="SELECT * FROM nycoffee.products WHERE product_name LIKE '%";
	
	public static final String DELETE_BY_ID ="DELETE FROM nycoffee.products WHERE id_product=?";
	
	public static final String INSERT ="INSERT INTO nycoffee.products (id_product, product_name, product_type, size, price) VALUES (?,?,?,?,?)";
	
	public static final String UPDATE ="UPDATE nycoffee.products SET id_product=?, product_name=?, product_type=?, size=?, price=? WHERE id_product=?";

}
