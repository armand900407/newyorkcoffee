package com.project.newyorkcoffee.queries;

public class ClosureQueries {
	
	public static final String ID_CLOSURE ="id_closure";
	
	public static final String EMPLOYEE_NAME ="employee_name";
	
	public static final String INIT_CASH ="init_cash";
	
	public static final String TOTAL_IN ="total_in";
	
	public static final String TOTAL_OUT ="total_out";
	
	public static final String TOTAL_CASH ="total_cash";
	
	public static final String TOTAL_CARDS ="total_cards";
	
	public static final String TOTAL_GENERATED ="total_generated";
	
	public static final String CLOSURE_DATE ="closure_date";
	
	public static final String FIND_ALL ="SELECT * FROM nycoffee.closure";
	
	public static final String FIND_BY_ID ="SELECT * FROM nycoffee.closure WHERE id_closure=?";
	
	public static final String DELETE_BY_ID ="DELETE FROM nycoffee.closure WHERE id_closure=?";
	
	public static final String INSERT ="INSERT INTO nycoffee.closure (id_closure, employee_name, init_cash, total_in, total_out, total_cash, total_cards, total_generated, closure_date) VALUES (?,?,?,?,?,?,?,?,?)";
	
	public static final String UPDATE ="UPDATE nycoffee.closure SET id_closure=?, employee_name=?, init_cash=?, total_in=?, total_out=?, total_cash=?, total_cards=?, total_generated=?, closure_date=? WHERE id_closure=?";
}
