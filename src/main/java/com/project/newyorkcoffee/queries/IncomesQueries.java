package com.project.newyorkcoffee.queries;

public class IncomesQueries {
	
	public static final String ID_INCOME ="id_income";
	
	public static final String NO_SALE ="no_sale";
	
	public static final String DESCRIPTION ="description";
	
	public static final String PAYMENT_TYPE ="payment_type";
	
	public static final String TOTAL ="total";
	
	public static final String INCOME_DATE ="income_date";
	
	public static final String EMPLOYEE ="employee";
	
	public static final String FIND_ALL ="SELECT * FROM nycoffee.incomes";
	
	public static final String FIND_TOTAL_CASH ="SELECT SUM(TOTAL) AS TOTAL FROM nycoffee.incomes WHERE payment_type = 'Efectivo' AND NOT (id_income = 1)";
	
	public static final String FIND_TOTAL_CARD ="SELECT SUM(TOTAL) AS TOTAL FROM nycoffee.incomes WHERE payment_type = 'Tarjeta'";
	
	public static final String FIND_BY_ID ="SELECT * FROM nycoffee.incomes WHERE id_income=?";
	
	public static final String DELETE_BY_ID ="DELETE FROM nycoffee.incomes WHERE id_income=?";
	
	public static final String INSERT ="INSERT INTO nycoffee.incomes (id_income,no_sale,description,payment_type,total,income_date,employee) VALUES (?,?,?,?,?,?,?)";
	
	public static final String UPDATE ="UPDATE nycoffee.incomes SET id_income=?, no_sale=?, description=?, payment_type=?, total=?, incomeDate=?, employee=? WHERE id_income=?";

	public static final String TRUNCATE_INCOMES ="TRUNCATE nycoffee.incomes";
	
	
}
