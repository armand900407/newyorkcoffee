package com.project.newyorkcoffee.queries;

public class AdministrationQueries {
	
	public static final String ID_EMPLOYEE ="id_employee";
			
	public static final String EMPLOYEE_TYPE ="employee_type";
	
	public static final String EMPLOYEE_NAME ="employee_name";
	
	public static final String EMPLOYEE_LAST_NAME ="employee_last_name";
	
	public static final String EMPLOYEE_PHONE ="phone";
	
	public static final String PASSWORD ="pass";
	
	public static final String FIND_ALL ="SELECT * FROM nycoffee.administration";
	
	public static final String FIND_BY_ID ="SELECT * FROM nycoffee.administration WHERE id_employee=?";
	
	public static final String FIND_BY_AUTHENTICATION ="SELECT * FROM nycoffee.administration WHERE employee_name=? AND pass=?";
	
	public static final String DELETE_BY_ID ="DELETE FROM nycoffee.administration WHERE id_employee=?";
	
	public static final String INSERT ="INSERT INTO nycoffee.administration (id_employee, employee_type, employee_name, employee_last_name, phone, pass)"
			+ " VALUES (?,?,?,?,?,?)";
	
	public static final String UPDATE ="UPDATE nycoffee.administration SET id_employee=?, employee_type=?, employee_name=?,"
			+ " employee_last_name=?, phone=?, pass=? WHERE id_employee=?";
	

}
