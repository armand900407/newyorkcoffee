package com.project.newyorkcoffee.conection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

public class DbConnection {

	private static DbConnection connection;
	
	public static Connection connect;
    
	//@Value("${spring.datasource.url}")
	public static String source="jdbc:mysql://127.0.0.1:33061/nycoffee?useSSL=false";
	
	public static String root="root";
	
	public static String password="mysql";
    
    private DbConnection(Connection connect, String source ,String root, String password) {
    	this.connect = connect;
    	this.source = source;
    	this.root = root;
    	this.password = password;
    }
    
    public static DbConnection getInstance(String source ,String root, String password) {
    	if(connection == null) {
    		connect = DbConnection.getConexion(source, root, password);
    		connection = new DbConnection(connect,source, root, password);
    	}
    	return connection;
    }

	public static Connection getConexion(String source ,String root, String password){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            //connect = DriverManager.getConnection("jdbc:mysql://127.0.0.1:33061/nycoffee?useSSL=false","root2","mysql2");
            connect = DriverManager.getConnection("jdbc:mysql://127.0.0.1:33061/nycoffee?useSSL=false","root","mysql");
            //connect = DriverManager.getConnection(source,root,password);
            JOptionPane.showMessageDialog(null,"Successful Conexion for NewYork CoffeeDataBase!");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Db Communication Link Failure, Make Sure DataBase is On: "+e);
        }
        return connect;
    }
    
    public void disconnectDb() throws SQLException{
    connect.close();
   }
    
    public static void main(String[] args) {
    	DbConnection o = new DbConnection(connect,"jdbc:mysql://127.0.0.1:33061/nycoffee?useSSL=false","root","mysql");
     
    }
}
