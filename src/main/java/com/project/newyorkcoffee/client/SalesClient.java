package com.project.newyorkcoffee.client;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.project.newyorkcoffee.model.Sales;
import com.project.newyorkcoffee.model.TemporarySale;

public class SalesClient {
	
	public static Sales getSaleById(Integer idSale) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/sales/getSale/{id}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Sales> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Sales.class, idSale);
        Sales sale = responseEntity.getBody(); 
        return sale;
    }
	
	public static Sales[] getSaleByNoSale(Integer noSale) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/sales/getNoSale/{id}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Sales[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Sales[].class, noSale);
        Sales[] sale = responseEntity.getBody(); 
        return sale;
    }
	
    public static Sales[] getAllSales() {
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/sales/getAll";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Sales[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Sales[].class);
        Sales[] sales = responseEntity.getBody();
        return sales;
    }
    
    public static Integer addSale(final Sales sales) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        int status=200;
        String url = "http://localhost:8080/newyorkcoffee/v1/sales/createSale";
        HttpEntity<Sales> requestEntity = new HttpEntity<Sales>(sales, headers);
        try {
        URI uri = restTemplate.postForLocation(url, requestEntity);
        }catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
        	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
        	      status = httpClientOrServerExc.getRawStatusCode();
        	    }
        	return status;
        }
        return status; 
    }
    
    public static void updateSale(final Sales sale) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/sales/updateSale";
        HttpEntity<Sales> requestEntity = new HttpEntity<Sales>(sale, headers);
        restTemplate.put(url, requestEntity); 
    }
    
    public static Integer deleteSale(Integer idSale) {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/sales/deleteSale/{id}";
        HttpEntity<Sales> requestEntity = new HttpEntity<Sales>(headers);
        try {
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, idSale); 
        }catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
        	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
        	      status = httpClientOrServerExc.getRawStatusCode();
        	    }
        	return status;
        }
        return status; 
    }
    
    public static Integer deleteByNoSale(Integer noSale) {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/sales/deleteNoSale/{noSale}";
        HttpEntity<Sales> requestEntity = new HttpEntity<Sales>(headers);
        try {
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, noSale); 
        }catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
        	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
        	      status = httpClientOrServerExc.getRawStatusCode();
        	    }
        	return status;
        }
        return status; 
    }
    
    public static Integer truncateSales() {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/sales/truncateSales";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        HttpEntity<TemporarySale> responseEntity = new HttpEntity<>(headers);
        try {
        restTemplate.exchange(url, HttpMethod.GET, requestEntity, Void.class);
    	}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    	      status = httpClientOrServerExc.getRawStatusCode();
    	    }
    	return status;
    }
    return status; 
    }
}
