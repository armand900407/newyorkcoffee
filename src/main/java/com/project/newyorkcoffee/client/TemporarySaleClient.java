package com.project.newyorkcoffee.client;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.project.newyorkcoffee.model.TemporarySale;


public class TemporarySaleClient {

	public static TemporarySale getTempSaleById(Integer idTempSale) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/tempsale/getSale/{id}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<TemporarySale> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, TemporarySale.class, idTempSale);
        TemporarySale tempSale = responseEntity.getBody(); 
        return tempSale;
    }
	
    public static TemporarySale[] getAllTempSales() {
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/tempsale/getAll";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<TemporarySale[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, TemporarySale[].class);
        TemporarySale[] tempSales = responseEntity.getBody();
        return tempSales;
    }
    
    public static void addTempSale(final TemporarySale tempSale) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/tempsale/createTempSale";
        HttpEntity<TemporarySale> requestEntity = new HttpEntity<TemporarySale>(tempSale, headers);
        URI uri = restTemplate.postForLocation(url, requestEntity);
        //System.out.println("URI GETPATH POST: "+uri.getPath());    	
    }
    
    public static void updateTempSale(final TemporarySale tempSale) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/tempsale/updateTempSale";
        HttpEntity<TemporarySale> requestEntity = new HttpEntity<TemporarySale>(tempSale, headers);
        restTemplate.put(url, requestEntity);
        System.out.println("restTemplate Updated: "+restTemplate); 
    }
    
    public static void deleteTempSale(Integer idTempSale) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/tempsale/deleteTempSale/{id}";
        HttpEntity<TemporarySale> requestEntity = new HttpEntity<TemporarySale>(headers);
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, idTempSale); 
    }
    
    public static Integer truncateTempSale() {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/tempsale/truncateTempSale";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        HttpEntity<TemporarySale> responseEntity = new HttpEntity<>(headers);
        try {
        restTemplate.exchange(url, HttpMethod.GET, requestEntity, Void.class);
    	}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    	      status = httpClientOrServerExc.getRawStatusCode();
    	    }
    	return status;
    }
    return status; 
    }
    
}
