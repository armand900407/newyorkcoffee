package com.project.newyorkcoffee.client;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.project.newyorkcoffee.model.Administration;

public class AdministrationClient {
	
	public static Administration getAdminById(Integer idAdm) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/admin/getAdm/{id}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Administration> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Administration.class, idAdm);
        Administration administrator = responseEntity.getBody(); 
        return administrator;
    }
	
	public static Administration getAdminAuthentication(String user,String pass) {
    	HttpHeaders headers = new HttpHeaders();
    	Administration administrator = null;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        int status=200;
        String url = "http://localhost:8080/newyorkcoffee/v1/admin/getAdmAuth/{user}/{pass}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        try {
        ResponseEntity<Administration> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Administration.class, user, pass);
        administrator = responseEntity.getBody();
        }catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
        	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
        	      status = httpClientOrServerExc.getRawStatusCode();
        	      administrator=null;
        	    }
        	return administrator;
        }
        return administrator;
    }
	
    public static Administration[] getAllAdmins() {
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/admin/getAll";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Administration[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Administration[].class);
        Administration[] administrator = responseEntity.getBody();
        return administrator;
    }
    
    public static Integer addAdmin(final Administration administrator) {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/admin/createAdm";
        try {
        HttpEntity<Administration> requestEntity = new HttpEntity<Administration>(administrator, headers);
        URI uri = restTemplate.postForLocation(url, requestEntity);  
        }catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    		if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    			status = httpClientOrServerExc.getRawStatusCode();
    		}
    		return status;
    		}
    	return status; 
    }
    
    public static Integer updateAdmin(final Administration administrator) {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/admin/updateAdm";
        try {
        HttpEntity<Administration> requestEntity = new HttpEntity<Administration>(administrator, headers);
        restTemplate.put(url, requestEntity);
    	}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    		if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    			status = httpClientOrServerExc.getRawStatusCode();
    		}
    		return status;
    		}
    	return status; 
    }
    
    public static Integer deleteAdmin(Integer idAdm) {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/admin/deleteAdm/{id}";
        HttpEntity<Administration> requestEntity = new HttpEntity<Administration>(headers);
        try {
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, idAdm);
    	}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    		if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    	      status = httpClientOrServerExc.getRawStatusCode();
    	    }
    	return status;
    }
    return status; 
    }
   
}
